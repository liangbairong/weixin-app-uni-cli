## 项目说明
统一打包目录：  
修改一下 package.json 文件的打包命令，增加下面参数： 

参数： UNI_OUTPUT_DIR=你的指定目录  

```json
 "dev:mp-weixin": "cross-env NODE_ENV=development UNI_PLATFORM=mp-weixin UNI_OUTPUT_DIR=dist/build/mp-weixin vue-cli-service uni-build --watch",
```
 

已集成EditorConfig，Prettier，eslint

WebStorm 不需要安装额外插件
vscode需要去插件市场安装插件：
- Prettier 下载插件 Prettier - Code formatter 
- EditorConfig 下载插件 EditorConfig for VS Code
- ESLint 下载插件 ESLint 
