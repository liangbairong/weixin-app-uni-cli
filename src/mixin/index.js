import ROOT from '@/config/index'
import { mapState } from 'vuex'

export default {
  data() {
    return {
      $IMG_URL: ROOT.IMG_URL

    }
  },
  computed: {
    ...mapState([
      'userInfo'
    ])
  },
  // onLoad() {
  //   this.minInit()
  // },
  created() {

  },
  methods: {

  }
}
