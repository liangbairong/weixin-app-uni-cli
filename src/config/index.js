
const ROOT = {
  sit: {
    BASE_URL: 'https://xxx/',
    IMG_URL: 'https://xxx/'
  },
  uat: {
    BASE_URL: 'https://xxx/',
    IMG_URL: 'https://xxx/'
  },
  prod: {
    BASE_URL: 'https://xxx/',
    IMG_URL: 'https://xxx/'
  }
}

export default ROOT[process.env.VUE_APP_ENV]
