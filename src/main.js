import Vue from 'vue'
import App from './App.vue'
import store from './store'
import uView from './uview-ui'
import http from './api/http'
// iconfont 组件
import Iconfont from './components/iconfont/iconfont.vue'
import min from './mixin/index'

Vue.use(uView)
Vue.mixin(min)

Vue.prototype.$store = store
Vue.config.productionTip = false

Vue.prototype.$http = http

Vue.component('l-icon', Iconfont)
App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
